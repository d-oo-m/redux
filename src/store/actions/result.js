import { DELETE_RESULT, STORE_RESULT } from "./actionTypes";
import { updateObject } from '../utility';

export const saveResult = (result ) => {
  return {
    type: STORE_RESULT,
    result: result
  }
}

export const storeResult = (result) => {
  return (dispatch, getState) => {
    setTimeout(() => {
      const oldCounter = getState().ctr.counter;
      console.log(oldCounter);
      dispatch(saveResult(result))
    }, 2000);
  }
};

export const deleteResult = (id) => {
  return {
    type: DELETE_RESULT,
    id: id
  }
};